﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mechanics : MonoBehaviour
{
    public GameObject calculator;
    public GameObject table;
    public Util util;

    public void activateCalculator() {
        util.activateGameObject(calculator);
        util.activateGameObject(table);
    }


}
