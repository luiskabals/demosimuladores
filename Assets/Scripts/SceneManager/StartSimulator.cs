﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartSimulator : MonoBehaviour
{

    public GameObject title;
    public GameObject border;
    public GameObject back;
    public GameObject subTitle;
    public GameObject unityImage;
    public GameObject unityText;
    public GameObject allItemsScene;
    public GameObject mecanicItems;
    public GameObject fade;
    public GameObject buttonsGroup;
    public GameObject[] buttons;
    [SerializeField, Range(0.0f, 3.0f)]
    private float speed;
    public Util util;
    // Start is called before the first frame update
    void Start()
    {
        
        deactivateAll();
        StartCoroutine(fadeON(0));
        StartCoroutine(activateAll(speed+0.5f));
        StartCoroutine(borderON(speed+0.5f));
        StartCoroutine(unityTextON(speed +0.7f));
        StartCoroutine(unityImageON(speed+1));
        StartCoroutine(titleON(speed + 1.5f)) ;
        StartCoroutine(subTitleON(speed + 2f)) ;
        StartCoroutine(mechanicsOn(speed + 4f));
        StartCoroutine(buttonGroupOn(speed + 4.2f));
        StartCoroutine(buttonsOn(speed + 4.5f));
    }

    private IEnumerator activateAll(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        util.activateGameObject(allItemsScene);
    }
    public void deactivateAll() {
        util.deactivateGameObject(buttonsGroup);
        foreach (GameObject button in buttons)
        {
            util.deactivateGameObject(button);
        }
        util.deactivateGameObject(mecanicItems);
        util.deactivateGameObject(allItemsScene);
        util.deactivateGameObject(fade);
        util.deactivateGameObject(border);
        util.deactivateGameObject(title);
        util.deactivateGameObject(subTitle);
        util.deactivateGameObject(unityImage);
        util.deactivateGameObject(unityText);
        util.deactivateGameObject(back);
        util.deactivateGameObject(unityText);
    }
    private IEnumerator buttonsOn(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        foreach (GameObject button in buttons) { 
        util.activateAnimator(button, "INTRO");
        }
    }
    private IEnumerator buttonGroupOn(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        util.activateAnimator(buttonsGroup, "INTRO");
    }
    private IEnumerator mechanicsOn(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        util.activateAnimator(mecanicItems, "INTRO");
    }
    private IEnumerator backON(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        util.activateAnimator(border, "INTRO");
    }

    private IEnumerator borderON(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        util.activateAnimator(border, "INTRO");
    }
    private IEnumerator fadeON(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        util.activateAnimator(fade, "ON");
    }

    private IEnumerator titleON(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        util.activateAnimator(title,"INTRO");
    }
    private IEnumerator subTitleON(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        util.activateAnimator(subTitle, "ON");
    }
    private IEnumerator unityImageON(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        util.activateAnimator(unityImage, "ON");
    }
    private IEnumerator unityTextON(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        util.activateAnimator(unityText, "ON");

    }


    //NO VA
public void showCheckmark(GameObject checkmark) {
        util.deactivateGameObject(checkmark);
        util.activateGameObject(checkmark);
    }

    public void showCross(GameObject cross)
    {
        util.deactivateGameObject(cross);
        util.activateGameObject(cross);
    }

}
