﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartScene : MonoBehaviour
{
    public GameObject intro;
    public GameObject fade;
    public GameObject pointer;
    public GameObject button;
    public GameObject logo;
    public Util util;
    [SerializeField, Range(0.0f, 3.0f)]
    private float speed;
    public Color32[] colours;

    public void Start()
    {
        SetBackgroundColour();
        DeactivateAll();
        StartTheScene();
    }
    public void SetBackgroundColour() {
        fade.GetComponent<Image>().color = colours[Random.Range(0,colours.Length)];
    }

    public void DeactivateAll() {
        util.deactivateGameObject(intro);
        util.deactivateGameObject(fade);
        util.deactivateGameObject(button);
        util.deactivateGameObject(pointer);
        util.deactivateGameObject(logo);
    }
    public void StartTheScene()
    {
        StartCoroutine(introON(0));
        StartCoroutine(backgroundON(speed));
        StartCoroutine(buttonINTRO(speed + 1));
        StartCoroutine(pointerON(speed + 2));
    }
    public void StartSimulator() {
        StartCoroutine(pointerOff(speed));
        StartCoroutine(buttonOff(speed+0.2f));
        StartCoroutine(sceneOff(speed + 0.6f));
        StartCoroutine(loadScene(speed + 2f));
    }
    private IEnumerator introON(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        util.activateGameObject(intro);
    }
    private IEnumerator buttonINTRO(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        util.activateGameObject(logo);
        util.activateAnimator(button,"INTRO");
    }
    private IEnumerator pointerON(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        util.activateAnimator(pointer, "ON");

    }
    private IEnumerator backgroundON(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        util.activateAnimator(fade, "ON");
    }

    private IEnumerator buttonOff(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        util.activateAnimator(button, "OFF");
    }
    private IEnumerator pointerOff(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        util.activateAnimator(pointer, "NONE");

    }
    private IEnumerator sceneOff(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        util.activateAnimator(fade, "OFF");
    }

    private IEnumerator loadScene(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        util.loadScene("Demo");
    }

    public void ChangeScene() {
        StartSimulator();

    }

}
