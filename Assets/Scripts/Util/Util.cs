﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Util : MonoBehaviour
{

    public void deactivateGameObject(GameObject gameobject) {
        gameobject.SetActive(false);
    }

    public void activateGameObject(GameObject gameobject)
    {
        if (gameobject)
        {
            gameobject.SetActive(true);
        }
        else {
            Debug.Log("gameobject  null!!!!!!!!!!!!");
        }
    }
    public void activateAnimator(GameObject gameobject,string animatorName) {
        if (!gameobject.activeInHierarchy) {
            gameobject.SetActive(true);
        }
        if (gameobject.GetComponent<Animator>())
        {
            gameobject.GetComponent<Animator>().SetTrigger(animatorName);
        }
        else {
            Debug.Log("WARNING!!!!!!!! NO ANIMATOR "+ gameobject.name);
        }
    }
    public void loadScene(string sceneName) { 
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName);

    }
}
